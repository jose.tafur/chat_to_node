class HelloChannel < ApplicationCable::Channel
  def subscribed
  	p "Cliente en node solicito suscribirse"
    stream_from "hello_channel"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end

  def send_message(data)
  	p "ENVIANDO MENSAJE: #{data['data']['message']}"
  	ActionCable.server.broadcast 'hello_channel', message: "Este es un mensaje desde rails"
  end
end
