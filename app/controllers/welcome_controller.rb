class WelcomeController < ApplicationController

	def index
		ActionCable.server.broadcast 'hello_channel', message: "Este es un mensaje desde el controller de rails"
	end
end